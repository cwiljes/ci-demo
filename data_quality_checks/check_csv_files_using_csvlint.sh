#!/bin/sh

## The ruby that comes with Debian Jessie is too old, so let's switch:
source /etc/profile.d/rvm.sh
rvm use ruby-2.2

find -type f -iname "*.csv" | \
while read csv_file_name
do
	if ! csvlint "$csv_file_name"
	then
		echo "Warnung: $csv_file_name hat die Pruefung durch csvlint nicht bestanden."
		echo "check_scv_files warning Warning: $csv_file_name did not pass the test" | mail -s "Hello World Status Mail" cwiljes@cit-ec.uni-bielefeld.de
		exit 1
	fi
done
