#!/bin/sh

find -type f -iname "*.csv" | \
while read csv_file_name
do
  echo "helloworld eMail Warning: $csv_file_name did not pass the test" | mail -s "Hello World Status Mail" cwiljes@cit-ec.uni-bielefeld.de
done
