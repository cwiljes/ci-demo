#!/bin/sh

find -type f -iname "*.pdf" | \
while read pdf_file_name
do
	if ! pdfinfo "$pdf_file_name" | grep -q '^Encrypted:\s*no'
	then
		echo "Warnung: $pdf_file_name ist so nicht zur Archivierung geeignet, weil sie verschlüsselt ist!"
		exit 1
	fi
done
