﻿This is a fork of https://gitlab.ub.uni-bielefeld.de/cpietsch/ci-demo/

# Proje
[![pipeline status](https://gitlab.ub.uni-bielefeld.de/cwiljes/ci-demo/badges/master/pipeline.svg)](https://gitlab.ub.uni-bielefeld.de/cpietsch/ci-demo/commits/master)

# Hintergrund
Einige Informationen über GitLab und dessen Einsatz an der UniversitÃ¤tsbibliothek finden Sie im [Wiki](https://gitlab.ub.uni-bielefeld.de/cpietsch/ci-demo/wikis/home) dieses Projekts.

# Aufgabe
Sorgen Sie dafür, dass der rote Knopf (Badge) unter "Projektstatus" grün wird!

# Hilfestellung
Suchen Sie die Ursache für das Fehlschlagen der automatischen Tests! Sie können entweder in der Navigation auf CI/CD klicken und Jobs oder Pipelines untersuchen, oder Sie klicken auf den roten Knopf und dann auf das rote Kreuz neben dem letzten Commit und dann auf das neben "Stage".

# Nützliche Links
+ Getting started with GitLab CI: https://gitlab.ub.uni-bielefeld.de/help/ci/README.md
+ ELAG Workshop: Automatic quality feedback for inter-disciplinary research data management: http://elag2017.org/?option=com_sppagebuilder&view=page&id=31
+ Continuous Analysis: https://greenelab.github.io/continuous_analysis/
+ DFG project CONQUAIRE: http://conquaire.uni-bielefeld.de/

